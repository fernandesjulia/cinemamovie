
import javax.swing.*;
import static javax.swing.UIManager.get;

public class Menu {

    public static void main(String[] args) {
        int opcao, quantSalas = 0, i, j, quantFilmes = 0, tipo, quantFuncionario = 0,quantM = 0,quantI = 0,valorM = 0,valorI = 0,valorT = 0,valorF = 0;
        int tempo;
        boolean ocup = true;
        String type;
        SalaCinema v[] = new SalaCinema[1000];
        Filme x[] = new Filme[1000];
        Funcionario z[] = new Funcionario[1000];
        Cliente c[]= new Cliente[1000];
        String nome;
        do {
            opcao = Integer.parseInt(JOptionPane.showInputDialog(null,
                    "1.Realizar compra de ingresso \n"
                    + "2.Cadastrar filme \n"
                    + "3.Cadastrar salas de cinema \n"
                    + "4.Cadastrar funcionario \n"
                    + "5.Cadastrar cliente \n"
                    + "6.Obter relatório de filmes\n"
                    + "7.Obter relatório de salas de cinema\n"
                    + "8.Obter relatório de funcionários\n"
                    + "9.Obter relatório de clientes\n"
                    + "10.Sair"));
            switch (opcao) {
                case 1:
                    nome = JOptionPane.showInputDialog(null, "Informe o nome do filme pertencente ao ingresso: ");
                    for (i = 0; i < x.length; i++) {
                        if (nome.equalsIgnoreCase(x[i].getNome())) {
                            for (j = 0; j < v.length; j++) {
                                if (x[i].getSala() == v[i].getNumero()) {

                                    do {
                                        tipo = Integer.parseInt(JOptionPane.showInputDialog(null, "Informe o tipo de ingresso que você deseja comprar: \n"
                                                + "Digite 1 para meia \n"
                                                + "Digite 2 para inteira"));
                                        if (tipo == 1) {
                                            quantM = quantM + 1;
                                            JOptionPane.showMessageDialog(null, "Compra realizada! Valor: R$:" + x[i].getPrecoMeia());
                                            valorM = quantM * x[i].getPrecoMeia();
                                        }
                                        if (tipo == 2) {
                                            quantI = quantI + 1;
                                            JOptionPane.showMessageDialog(null, "Compra realizada! Valor: R$:" + x[i].getPrecoInteira());
                                            valorI = quantI * x[i].getPrecoInteira();
                                        }
                                    } while (tipo != 1 && tipo != 2);
                                    valorF = valorM + valorI;
                                    valorT = valorT + valorF;
                                }
                            }

                        }

                    }
                    break;
                case 2:
                    quantFilmes = Integer.parseInt(JOptionPane.showInputDialog(null, "Informe quantos filmes você deseja cadastrar"));
                    for (i = 0; i < quantFilmes; i++) {
                    	 nome = JOptionPane.showInputDialog(null, "Informe o nome do filme");
                    	 tempo = Integer.parseInt(JOptionPane.showInputDialog(null, "Informe o tempo do filme(em minutos)"));
                    	 int precoInt = Integer.parseInt(JOptionPane.showInputDialog(null, "Informe o preço do ingresso (inteira)"));
                    	 int precoMeia=Integer.parseInt(JOptionPane.showInputDialog(null, "Informe o preço do ingresso (meia)"));
                    	 int numSala= Integer.parseInt(JOptionPane.showInputDialog(null, "Informe o número da sala de cinema onde o filme irá passar"));
                    	 String tipoExibicao=JOptionPane.showInputDialog(null, "Informe o tipo de exibição do filme: ");
                    	 String Fornecedor=JOptionPane.showInputDialog(null, "Informe o fornecedor do filme: ");
                    	 x[i] = new Filme(nome, precoInt, precoMeia, tempo, numSala, tipoExibicao, Fornecedor);

                    }break;

                case 3:
                    quantSalas = Integer.parseInt(JOptionPane.showInputDialog(null, "Informe quantas salas de filme seu cinema possui"));
                    for (i = 0; i < quantSalas; i++) {
                    	int sala=Integer.parseInt(JOptionPane.showInputDialog(null, "Informe o número da sala"));
                        int Fileiras=Integer.parseInt(JOptionPane.showInputDialog(null, "Informe o número de  fileiras de poltronas que essa sala possui"));
                        int Colunas=Integer.parseInt(JOptionPane.showInputDialog(null, "Informe o número de  colunas de poltronas que essa sala possui"));
                        v[i]= new SalaCinema(sala, Fileiras, Colunas);
                        JOptionPane.showMessageDialog(null, "Sala " + v[i].getNumero() + " cadastrada com sucesso!");
                    } break;

                case 4:
                    quantFuncionario = Integer.parseInt(JOptionPane.showInputDialog(null, "Informe quantos funcionários você deseja cadastrar"));
                    for (i = 0; i < quantFuncionario; i++) {
                        nome = JOptionPane.showInputDialog(null, "Informe o nome do(a) funcionário(a)");
                        int dias_por_mes= Integer.parseInt(JOptionPane.showInputDialog(null, "Informe quantos dias por mês ele(a) trabalha"));
                        int horas_por_dia = Integer.parseInt(JOptionPane.showInputDialog(null, "Informe quantas horas por dia ele(a) trabalha"));
                        int salario_hr = Integer.parseInt(JOptionPane.showInputDialog(null, "Informe quantos reais ele(a) ganha por hora"));
                        z[i]= new Funcionario(nome,dias_por_mes, horas_por_dia,salario_hr);
                        JOptionPane.showMessageDialog(null, "Funcionário(a) " + z[i].getNome() + " cadastrado(a) com sucesso!");
                    }break;

                case 5:
                	int quantCliente= Integer.parseInt(JOptionPane.showInputDialog(null, "Informe quantos clientes serão cadastrados"));
                	 for (i = 0; i < quantCliente; i++) {
                		 String name=JOptionPane.showInputDialog(null, "Informe o nome do(a) cliente");
                		 String telefone=JOptionPane.showInputDialog(null, "Informe o telefone do(a) cliente");
                		 String cpf=JOptionPane.showInputDialog(null, "Informe o CPF do(a) cliente");
                		 String idade = JOptionPane.showInputDialog(null, "Informe a idade do(a) cliente");
                                 String email = JOptionPane.showInputDialog(null, "Informe o email do(a) cliente");
                		 c[i]= new Cliente(name, telefone, cpf, idade,email);
                		 JOptionPane.showMessageDialog(null, "Cliente " + c[i].getNome() + " cadastrado(a) com sucesso!");
                	 }break;
                case 6:

                    for (i = 0; i < x.length; i++) { 
                    	JOptionPane.showMessageDialog(null, x[i].toStringFilme());
                    }break;
                    
                case 7:

                    for (i = 0; i < x.length; i++) { 
                    	JOptionPane.showMessageDialog(null,v[i].toStringSalaCinema());
                    }break;
                    
                case 8:

                    for (i = 0; i < x.length; i++) { 
                    	JOptionPane.showMessageDialog(null,z[i].toStringFuncionario());
                    }break;
                    
                case 9:

                    for (i = 0; i < x.length; i++) { 
                    	JOptionPane.showMessageDialog(null,c[i].toStringCliente());
                    }break;


                //testar opcao invalida      
            }
        } while (opcao !=10 );

    }

}
