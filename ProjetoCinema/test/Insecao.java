
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Insecao {
    public void inserirCliente(String nomecli,String cpf,String telefone,String idade,String email ){
        try {
            Connection c = Conexao.ObterConexao();
            System.out.println(c);
            PreparedStatement ps=c.prepareStatement("INSERT INTO SC_CINEMA.CLIENTE(nomecli,cpf,telefone,idade,email) "
                    + "VALUES(?,?,?,?,?)");
            ps.setString(1,nomecli);
            ps.setString(2, cpf);
            ps.setString(3,telefone);
            ps.setInt(4, Integer.valueOf(idade));        
            ps.setString(5,email);
            ps.execute();
        } catch (SQLException ex) {
            Logger.getLogger(Insecao.class.getName()).log(Level.SEVERE, null, ex);
        }
                }

     
}
