
public class Funcionario {

    private String nome;
    private int diasTrabalho, horaTrabalho, salario_por_hora, salario_final;

//CONSTRUTOR
    public Funcionario(String nome, int diasTrabalho, int horaTrabaho, int salario_por_hora) {
    	this.nome=nome;
    	this.diasTrabalho=diasTrabalho;
    	this.horaTrabalho=horaTrabalho;
    	this.salario_por_hora= salario_por_hora;
    	this.salario_final=salario_por_hora * horaTrabalho * diasTrabalho;
    }
    
  //toString
    public String toStringFuncionario () {
    	return("Nome do funcioário: " + nome 
    			+ "\n Dias de trabalho(por mês): " + diasTrabalho
    			+ "\n Horas de trabalho(por dia): " + horaTrabalho
    			+ "\n Salário por hora: " +salario_por_hora
    			+ "\n Salário final: " + salario_final);
    }


//SET GET NOME
    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }
    
// SET GET  NUMERO DE DIAS DE TRABALHO POR MES

    public void setDiasTrabalho(int diasTrabalho) {
        this.diasTrabalho = diasTrabalho;
    }
   
    public int getDiasTrabalho() {
    	return diasTrabalho;
    }
    
// SET GET HORAS DE TRABALHO POR DIA

    public void setHoraTrabalho(int horaTrabalho) {
        this.horaTrabalho = horaTrabalho;
    }
    
    public int getHoraTrabalho() {
    	return horaTrabalho;
    }
    
 // SET GET SALARIO POR HORA

    public void setSalario_por_hora(int salario_por_hora) {
        this.salario_por_hora = salario_por_hora;
    }
    
    public int getSalario_por_hora() {
    	return salario_por_hora;
    }

// RETORNO DO SALARIO FINAL DO FUNCIONARIO
    public int getSalario_final() {
        salario_final = salario_por_hora * horaTrabalho * diasTrabalho;
        return salario_final;
    }
}
