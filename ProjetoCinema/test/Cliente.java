
public class Cliente {
	String nomeCli, telefone, cpf, retorno, email;
	String idade;
        int quantFilmesAssistidos;
	
//CONSTRUTOR
	public Cliente(String nomeCli, String telefone, String cpf, String idade,String email) {
		this.nomeCli= nomeCli;
		this.telefone=telefone;
		this.cpf=cpf;
		this.idade=idade;
                this.email=email;
		this.quantFilmesAssistidos=0;
	}

    Cliente() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
	
	//toString
    public String toStringCliente () {
    	return("Nome do(a) cliente: " + nomeCli
                        +"n E-mail:" + email
    			+ "\n Telefone: " + telefone
    			+ "\n CPF: " + cpf
    			+ "\n Idade: " + idade);
    }
			
//GET E SET NOME
	public void setNome(String nome) {
        this.nomeCli = nomeCli;
    }

    public String getNome() {
        return nomeCli;
    }
    
// GET E SET CPF
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getCpf() {
        return cpf;
    }
    
 // GET E SET IDADE
    public void setIdade(String idade) {
        this.idade = idade;
    }

    public String getIdade() {
        return idade;
    }
    
 // GET E SET IDADE
    public void setTel(String telefone) {
        this.telefone = telefone;
    }

    public String getTel() {
        return telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    
// CODIGO QUE POSSIBILITA UMA PROMOÇÃO AO CLIENTE
    public void compraCliente(int quantFilmes) {
    	quantFilmes++;
    }
    
    public String promoção(int quantFilmes, int quantFilmesPromocao ) {
    	if(quantFilmes ==  quantFilmesPromocao) {
    		quantFilmes=0;
    		retorno = "Você ganhou um ingresso!";
    	}
    	
    	else {
    		int falta = quantFilmesPromocao-quantFilmes;
    		retorno = "Faltam " + falta + "filmes para que você ganhe um ingresso de graça!";
    	}
    	
    	return retorno;
    }
}