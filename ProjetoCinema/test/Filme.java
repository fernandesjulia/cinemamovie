
public class Filme {

    private String nomeFilme;
    private int precoInt, tempo, precoMeia, sala, pagamento, dia, mes, ano, hora;
    private String exibicao, fornecedor;
    
//CONSTRUTOR
    
    public Filme(String nomeFilme, int precoInt, int tempo, int precoMeia, int sala, String categoria, String fornecerdor){
        this.nomeFilme= nomeFilme;
        this.precoInt= precoInt;
        this.precoMeia= precoMeia;
        this.tempo=tempo;
        this.sala=sala;
        this.fornecedor=fornecedor;
        this.exibicao=exibicao;
    }
    
//toString
    public String toStringFilme () {
    	return("Nome do filme: " + nomeFilme 
    			+ "\n Preço do ingresso (inteira): " + precoInt
    			+ "\n Preço do ingresso (meia): " + precoMeia
    			+ "\n Tempo do filme (em minutos): " +tempo
    			+ "\n Sala de exibição: " + sala
    			+ "\n Fornecedor: " + fornecedor
    			+"\n Tipo de exibição: " + exibicao);
    }

    
//SET GET NOME
    public void setNome(String nomeFilme) {
        this.nomeFilme = nomeFilme;
    }
    public String getNome() {
        return nomeFilme;
    }
    
// SET GET PRECOINTEIRA 
    public void setPrecoInteira(int precoInt) {
        this.precoInt = precoInt;
    }

    public int getPrecoInteira() {
        return precoInt;
    }

// SET GET PRECOMEIA

    public void setPrecoMeia(int precoMeia) {
        this.precoMeia = precoMeia;
    }

    public int getPrecoMeia() {
    	return precoMeia;
    }
    
// SET GET NUMERO DA SALA
    public void setSala(int sala) {
        this.sala = sala;
    }
    
    public int getSala() {
        return sala;
    }

// SET GET FORMA DE EXIBICAO
    
    public void setExibicao(String exibicao) {
    	this.exibicao= exibicao;
    }
    
    public String getExibicao() {
    	return exibicao;
    }

    
// SET GET FORNECEDOR
    
    public String getFornecedor() {
        return fornecedor;
    }
    public void setFornecedor(String fornecedor) {
    	this.fornecedor= fornecedor;
    }
    
    
//SET GET TEMPO DE DURACAO
    public void setTempo(int tempo) {
        this.tempo = tempo;
    }
    
    public int getTempo() {
    	return tempo;
    }
// PAGAMENTO PARA O FORNECEDOR
    public void agendaPagamento(int pagamento, int dia, int mes, int ano, int hora) {
    	this.pagamento=pagamento;
    	this.dia=dia;
    	this.hora=hora;
    	this.mes=mes;
    	this.ano=ano;
    }
    
    public String informaPagamento() {
    	String retorno= ("Valor: " + pagamento+ " Dia: " + dia + " Mês: " +mes+ " Ano: "+ ano + " Hora: " + hora);
    	return retorno;
    	
    	
    }

}
