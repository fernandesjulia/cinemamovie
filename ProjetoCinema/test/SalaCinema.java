
import javax.swing.*;

public class SalaCinema {
	
    private int quantFileiras, poltrona, quantColunas, poltronas[][], coluna, fileira, numero;  //quantFileiras, quantColunas, coluna, fileira  ???
    private String cadeiraDisponivel, disponibilidade;  // não precisa. Marque a disponibilidade na matriz Poltronas
    private boolean ocupada;
    private boolean polt = true;  //tirar
    
//CONSTRUTOR    
    public SalaCinema(int numero, int quantFileiras, int quantColunas) {
    	this.numero=numero;
    	this.quantFileiras=quantFileiras;
    	this.quantColunas= quantColunas;
    	
    }

    //toString
    public String toStringSalaCinema () {
    	return("Número da sala: " + numero 
    			+ "\n Quantidade de fileiras: " + quantFileiras
    			+ "\n Quantidade de colunas: " + quantColunas);
    }

    
// GET E SET NUMERO DA SALA DE CINEMA
    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getNumero() {
        return numero;
    }
    
// GET E SET QUANTIDADE DE FILEIRAS DAS POLTRONAS DA SALA DE CINEMA
    public void setFileiras(int quantFileiras) {
        this.quantFileiras = quantFileiras;
    }

    public int getFileiras() {
        return quantFileiras;
    }
 
//GET E SET QUANTIDADE DE COLUNAS DAS POLTRONAS DA SALA DE CINEMA
    public void setColunas(int quantColunas) {
        this.quantColunas = quantColunas;
    }

    public int getColunas() {
        return quantColunas;
    }
    
//
    
    public boolean isOcupada() {
        return ocupada;
    }

    
    public void setOcupada(boolean ocupada) {
        this.ocupada = ocupada;
    }

    public void setPoltrona(int poltrona) {
        this.poltrona = poltrona;
    }

    public int getPoltrona() {
        return poltrona;
    }


}
