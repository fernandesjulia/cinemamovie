/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author berna
 */
public class CinemaEstabelecimento {

    String telefone, nome, cidade, bairro, rua;
    int numero;

    public CinemaEstabelecimento( String nome, String cidade, String bairro, String rua, int num,String tel) {
        this.bairro = bairro;
        this.cidade = cidade;
        this.nome = nome;
        this.numero = num;
        this.rua = rua;
        this.telefone = tel;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    @Override
    public String toString() {
        return "Cinema " + this.nome
                + "\n Endereço: " + this.bairro + "," + this.rua + "," + this.numero + "° ," + this.cidade
                + "\n Telefone:" + this.telefone;
    }
}
